-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: domaci_Mirjana
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Kupac`
--

DROP TABLE IF EXISTS `Kupac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kupac` (
  `idKupac` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(40) NOT NULL,
  `prezime` varchar(40) NOT NULL,
  `adresa` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idKupac`),
  UNIQUE KEY `idKupac_UNIQUE` (`idKupac`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kupac`
--

LOCK TABLES `Kupac` WRITE;
/*!40000 ALTER TABLE `Kupac` DISABLE KEYS */;
INSERT INTO `Kupac` VALUES (1,'Dragica','Dragas','Svetozara Miletica'),(2,'Milena ','Ivic','Stevana Doronjskog'),(3,'Jovana ','Mihajlovic','IV vojvodjanske brigade');
/*!40000 ALTER TABLE `Kupac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Narudzbenica`
--

DROP TABLE IF EXISTS `Narudzbenica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Narudzbenica` (
  `idNarudzbenica` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  PRIMARY KEY (`idNarudzbenica`),
  UNIQUE KEY `idNarudzbenica_UNIQUE` (`idNarudzbenica`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Narudzbenica`
--

LOCK TABLES `Narudzbenica` WRITE;
/*!40000 ALTER TABLE `Narudzbenica` DISABLE KEYS */;
INSERT INTO `Narudzbenica` VALUES (30,'2015-02-20'),(40,'2013-02-20'),(41,'2022-03-23');
/*!40000 ALTER TABLE `Narudzbenica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Proizvod`
--

DROP TABLE IF EXISTS `Proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Proizvod` (
  `IDProizvod` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(50) NOT NULL,
  `barkod` varchar(13) NOT NULL,
  `cena` decimal(2,0) NOT NULL,
  PRIMARY KEY (`IDProizvod`),
  UNIQUE KEY `IDProizvod_UNIQUE` (`IDProizvod`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Proizvod`
--

LOCK TABLES `Proizvod` WRITE;
/*!40000 ALTER TABLE `Proizvod` DISABLE KEYS */;
INSERT INTO `Proizvod` VALUES (1,'sladoled','2509980165070',99),(2,'majica','0408982185016',99);
/*!40000 ALTER TABLE `Proizvod` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-16 10:23:51
